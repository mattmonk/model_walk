//
//      Developed by Lucien West, Cardno (Qld) Pty Ltd - Civil Designer, Springfield
//      Version 1.0.1  18-04-2008	  Model walk macro
//      Version 2.0.1  25-10-2008	  Code trimmed to working components only, combined into one file.
//      Version 2.0.2  09-11-2008	  View_exits and View_create amended
//      Version 3.0.1  29-12-2008	  Model display filters, view models add/remove, model clean/delete, 
//                                        fast forward/rewind and model sorting added.
//

//Model walk macro.
#define DEBUG                       0
#define DEBUG_MOTION                0
#define INTERACTIVE                 0
#define CHECK_MODEL_MUST_EXIST      7
#define MODEL_EXISTS                2
#define CHECK_VIEW_MUST_EXIST       2
#define CHECK_VIEW_MUST_NOT_EXIST   25
#define VIEW_EXISTS                 7

//--------------------------------------------------------------------------------------
//------------------------------------Prototypes----------------------------------------
//--------------------------------------------------------------------------------------
//Debug Tools
void Print(Integer i, Text extension);
void Print(Text text, Text extension);
void Print(Dynamic_Text dtext, Text extension);
//Slf
Text get_pad();
Text outdent();
Text quote(Text text);
Text field(Text name,Text value);
Text field(Text prefix,Text name,Text value);
Text group(Text name,Text value);
Text group(Text prefix,Text name,Text value);
Text group();
//Quick Sort
void Qsort(Dynamic_Integer &list,Integer &index[],Integer number);
void Qsort(Dynamic_Text &list,Integer &index[],Integer number);
//General Text Tools.
Integer Find_text(Dynamic_Text texts,Text tofind);
Integer Get_sorted(Dynamic_Text list, Integer index[], Dynamic_Text &list_sorted);
//General Model Tools
Integer Model_clean(Model model);
Integer Model_clean(Dynamic_Text model_names);
Integer Model_delete(Dynamic_Text model_names);
//View Tools
Integer View_exists(Text &view_name,Text type,Integer mode);
Integer View_remove_model(View view,Text model_name);
Integer View_remove_model(View view, Dynamic_Text model_names);
Integer View_remove_model(View view);
Integer View_add_model(View view,Text model_name);
Integer View_add_model(View view, Dynamic_Text model_names);
Integer View_get_next(Text &number_name);
Integer Create_view(Text view_name,Text type);
Integer Create_view(Text &view_name,Text &type,Integer mode);
//Tin Tools
Integer Is_null(Tin tin);
Integer Is_supertin(Tin stin);
Tin Get_tin(Text model_name,Integer from_model);
//Message panels.
Integer Message_view_panel(Text title,Text message,Text &view_name);

//--------------------------------------------------------------------------------------
//----------------------------------Model Walk--------------------------------------
//--------------------------------------------------------------------------------------
Integer Get_project_model_names(Dynamic_Text &model_names, Text models_search, Integer &num_items, Choice_Box sort_box);
Integer Models_display_increment(Dynamic_Text &model_names_add, Integer increment,Integer &display_start_pos,Integer &display_end_pos,Dynamic_Text &model_names);
Integer Models_filter(Dynamic_Text &model_names_org, Integer_Box elt_box, Named_Tick_Box tin_tick, Named_Tick_Box stin_tick, Button show_elements, Message_Box filter_box);
void Models_data(Integer_Box model_pos_box, Model_Box model_box, Dynamic_Text model_names, Integer start, Integer end);
void Models_message(Message_Box message_box, Dynamic_Text model_names, Integer start, Integer end);
void Panel_Model_Walk_Ext(View view);

//Start CDD-Model_Walk_Ext
void main() {
  Integer my_return;

  Print("\nModel Walk\n");
//Get Model Walk View
  Text view_name ="Model Walk", view_type="Plan";
  my_return = Create_view(view_name,view_type,11);
  if (my_return==0){
//Start Panel
    View view = Get_view(view_name);
    Panel_Model_Walk_Ext(view);
  } else {
    Print("View failed, exiting\n");
  }
  //Delete_view(view_name);
}


void Panel_Model_Walk_Ext(View view)
{
//Create the panel
//
  Text macro_help = "help";
  Message_Box message_box = Create_message_box("");
//
  Panel panel = Create_panel("Model Walk V3-0");
  Vertical_Group vgroup = Create_vertical_group(0);
//
    Vertical_Group vgroup2 = Create_vertical_group(0);
    Set_border(vgroup2,"Model Display");
//Button Horixontal Group
     Horizontal_Group rowa3_group = Create_horizontal_group(0);
//Box Integer
           Integer_Box model_pos_box = Create_integer_box("Pos.",message_box);
           Set_help(model_pos_box,macro_help);
           Set_data(model_pos_box,0);
           Set_width_in_chars(model_pos_box,6);
           Append(model_pos_box,rowa3_group);
//Box Integer
           Integer_Box model_step_box = Create_integer_box(" Skip",message_box);
           Set_help(model_step_box,macro_help);
           Set_data(model_step_box,10);
           Set_width_in_chars(model_step_box,4);
           Append(model_step_box,rowa3_group);
         Append(rowa3_group,vgroup2);
//Button Horixontal Group
         Horizontal_Group rowa2_group = Create_horizontal_group(0);
//Button Fast Rewind
           Button rewind_button = Create_button("<<-","rewind");
           Set_help(rewind_button,macro_help);
           Set_width_in_chars(rewind_button,5);
           Append(rewind_button,rowa2_group);
//Button Model Previous
           Button previous_button = Create_button("<-","previous");
           Set_help(previous_button,macro_help);
           Set_width_in_chars(previous_button,4);
           Append(previous_button,rowa2_group);
//Button Model Next
           Button next_button = Create_button("->","next");
           Set_help(next_button,macro_help);
           Set_width_in_chars(next_button,4);
           Append(next_button,rowa2_group);
//Button Fast Forward
           Button forward_button = Create_button("->>","forward");
           Set_help(forward_button,macro_help);
           Set_width_in_chars(forward_button,5);
           Append(forward_button,rowa2_group);
         Append(rowa2_group,vgroup2);
//Model Box
      Model_Box model_box = Create_model_box("Model",message_box,1);
      Set_help(model_box,macro_help);
      Set_optional(model_box,1);
      Set_width_in_chars(model_box,16);
      Append(model_box,vgroup2);
//
         Horizontal_Group rowa1_group = Create_horizontal_group(0);
//Buttton Proccess
           Button clean_button = Create_button("Clean","clean");
           Set_help(clean_button,macro_help);
           Set_width_in_chars(clean_button,6);
           Append(clean_button,rowa1_group);
//Buttton Proccess
           Button delete_button = Create_button("Delete","delete");
           Set_help(delete_button,macro_help);
           Set_width_in_chars(delete_button,6);
           Append(delete_button,rowa1_group);
         Append(rowa1_group,vgroup2);
//Choice Box
          Choice_Box sort_box = Create_choice_box("Sort models by",message_box);
         Append(sort_box,vgroup2);
       Append(vgroup2,vgroup);
//
//Horixontal Group
    Vertical_Group vgroup1 = Create_vertical_group(0);
    Set_border(vgroup1,"Process to ");
//Box View
      View_Box view_add_box = Create_view_box("View",message_box,CHECK_VIEW_MUST_EXIST);
      Set_help(view_add_box,macro_help);
      Set_optional(view_add_box,1);
      Append(view_add_box,vgroup1);
      Set_width_in_chars(view_add_box,16);
//Button Horixontal Group
   Horizontal_Group hgroup1 = Create_horizontal_group(0);
//Buttton Proccess
      Button add_button = Create_button("Add","add");
      Set_help(add_button,macro_help);
      Set_enable(add_button,0);
      Set_width_in_chars(add_button,6);
      Append(add_button,hgroup1);
//Buttton Proccess
      Button remove_button = Create_button("Remove","remove");
      Set_help(remove_button,macro_help);
      Set_enable(remove_button,0);
      Set_width_in_chars(remove_button,6);
      Append(remove_button,hgroup1);
    Append(hgroup1,vgroup1);
  Append(vgroup1,vgroup);
//
    Vertical_Group vgroup3 = Create_vertical_group(0);
    Set_border(vgroup3,"Filter models");
//Box Integer
       Integer_Box elements_max_box = Create_integer_box("Elements >",message_box);
       Set_help(elements_max_box,macro_help);
       Set_data(elements_max_box,1000);
       Set_optional(elements_max_box,1);
       Set_width_in_chars(elements_max_box,11);
       Append(elements_max_box,vgroup3);
//Button Horixontal Group
     Horizontal_Group row7_group = Create_horizontal_group(0);
//Box Tick Toggle
       Named_Tick_Box ignore_tin_tick = Create_named_tick_box("Tins",0,"");
       Set_help(ignore_tin_tick,macro_help);
       Set_data(ignore_tin_tick,"true");
       Append(ignore_tin_tick,row7_group);
//Box Tick Toggle
       Named_Tick_Box ignore_stin_tick = Create_named_tick_box("SuperTins",0,"");
       Set_help(ignore_stin_tick,macro_help);
       Set_data(ignore_stin_tick,"true");
       Append(ignore_stin_tick,row7_group);
     Append(row7_group,vgroup3);
//Filtered Messages
       Message_Box filter_box = Create_message_box("");
     Append(filter_box,vgroup3);
//Box Tick Toggle
       Button show_elements = Create_button("Display filtered","filter");
       Set_help(show_elements,macro_help);
//       Set_enable(show_elements_tick,0);
       Append(show_elements,vgroup3);
//
   Append(vgroup3,vgroup);
//Box Message - Created before Widgets using
   Append(message_box,vgroup);
//Button Horixontal Group
     Horizontal_Group button_group = Create_button_group();
//Buttton Proccess
     Button reset_button = Create_button("Reset","reset");
     Set_width_in_chars(reset_button,6);
     Set_help(reset_button,macro_help);
     Append(reset_button,button_group);
//Button Finish
     Button finish_button = Create_finish_button("Finish","finish");
     Set_width_in_chars(finish_button,6);
     Set_help(finish_button,macro_help);
     Append(finish_button,button_group);
//Panel Finish
    Append(button_group,vgroup);
  Append(vgroup,panel);
// Display Panel
//
//  Integer wx = 100,wy = 100;
  Show_widget(panel);
//
//**** Note: treat these as global variables
  Integer num_items, update=0;
  //update bitwise modes:- 1=Do an update, 2=no filtering, 4=Update project models
  Integer display_start_pos = 0;
  Integer display_end_pos = 0;
  Integer models_increment = 0;
  Dynamic_Text model_names;
  //View view; //Model walk view, passed from main code.
//
  Text model_name, models_message;
  Model my_model;
  Integer my_int_return;
  Text my_text_return;
//Get and Set Data
  Text sort_by[3];
  sort_by[1]="Date Created";
  sort_by[2]="Alpha Numeric";
  sort_by[3]="No. Elements";
  Set_data(sort_box,3,sort_by);
  Set_data(sort_box,sort_by[1]);
  Get_project_model_names(model_names,"*",num_items,sort_box);
//Setup While loop to keep panel open
  Integer panel_run = 1;
  while (panel_run) {
    Integer id,ierr;
    Text cmd,msg;
    Wait_on_widgets(id,cmd,msg);
#if DEBUG
  if (cmd!="motion select"){
    Print("panel id  <"+To_text(id)+"> cmd <"+cmd+"> msg <"+msg+">\n");
  }
#endif
#if DEBUG_MOTION
  Print("panel id  <"+To_text(id)+"> cmd <"+cmd+"> msg <"+msg+">\n");
#endif
//first process the commands that are common to all wgits or are rarely processed by the wigit ID
    switch(cmd) {
//      case "keystroke" : {
//        continue;
//      }
      case "set_focus"  : {
       continue;
      } break;
      case "kill_focus" : {
       continue;
      } break;
      case "Help" : {
        Winhelp(panel,"4d.hlp",'a',msg);
        continue;
      } break;
    }
//process each event by the wigit id
//most wigits do not need to be processed until the PROCESS button is pressed
//only the ones that change the appearance of the panel need to be processed in this loop
    switch(id) {
//Panel Quit Exit
      case Get_id(panel) :{
        if(cmd == "Panel Quit") {
          Print("Panel Quit\n");
          panel_run = 0;
        }
        if(cmd == "Panel About") continue;
      } break;
//Finish Button Exit
      case Get_id(finish_button) : {
        Print("Normal Exit\n");
        panel_run = 0;
      } break;
//View Box
      case Get_id(view_add_box) : {
        View view_process;
        Text view_name;
        Get_data(view_add_box,view_name);
        Get_name(view,my_text_return);
        if(view_name==""  || view_name==my_text_return){
          Set_data(view_add_box,"");
          Set_enable(add_button,0);
          Set_enable(remove_button,0);
          if(view_name==my_text_return){
            Set_data(message_box,"Process to:- can not use \"" + my_text_return + "\"");
            Set_cursor_position(view_add_box);
          }
        } else {
          if(Validate(view_add_box,CHECK_VIEW_MUST_EXIST,view_process)==VIEW_EXISTS){
            Set_enable(add_button,1);
            Set_enable(remove_button,1);
          } else {
            Set_enable(add_button,0);
            Set_enable(remove_button,0);
          }
        }
      } break;
//Next Button
      case Get_id(next_button) : {
        models_increment=1;
        update=1;
      } break;
//Previous Button
      case Get_id(previous_button) : {
        models_increment=-1;
        update=1;
      } break;
//Forward Button
      case Get_id(forward_button) : {
        Get_data(model_step_box,my_text_return);
        From_text(my_text_return,my_int_return);
        models_increment=my_int_return;
        update=1;
      } break;
//Rewind Button
      case Get_id(rewind_button) : {
        Get_data(model_step_box,my_text_return);
        From_text(my_text_return,my_int_return);
        models_increment=my_int_return*-1;
        update=1;
      } break;
//Model Box
      case Get_id(model_box) : {
        switch (cmd) {
          case("model selected") : {
            models_increment=0;
            if(Validate(model_box,CHECK_MODEL_MUST_EXIST,my_model)== MODEL_EXISTS){
              update=9;
            } else {
              update=1;
            }
          } break;
        }
      } break;
//Choice Box
      case Get_id(sort_box) : {
        switch (cmd) {
          case("text selected") : {
            my_int_return = Validate(sort_box,my_text_return);
            Print(my_int_return,"Validate Choice");
            if(my_int_return==1){
              models_increment=0;
              update=5;
            } else {
              Set_data(message_box,"\""+my_text_return+"\" not a valid sort method");
            }
            Set_cursor_position(next_button);
          } break;
        }
      } break;
//Model Pos Box
      case Get_id(model_pos_box) : {
        switch (cmd) {
          case("integer selected") : {
            if(Validate(model_pos_box,my_int_return)){
              if(my_int_return > 0 && my_int_return <= num_items){
                display_start_pos = my_int_return;
                display_end_pos = my_int_return;
                models_increment=0;
                update=1;
                } else {
                Set_data(model_pos_box,display_end_pos);;
              }
            }
            Set_cursor_position(next_button);
          } break;
        }
      } break;
//Elements Max Box
      case Get_id(elements_max_box) : {
        switch (cmd) {
          case("integer selected") : {
            if(Validate(elements_max_box,my_int_return)){
              if(my_int_return <= 0){
                Set_data(elements_max_box,"");
                Set_cursor_position(elements_max_box);
              } else {
              }
              models_increment=0;
              update=1;
            }
          } break;
        }
      } break;
//Ignore Tin Tick
      case Get_id(ignore_tin_tick) : {
        models_increment=0;
        update=1;
      } break;
//Ignore SuperTin Tick
      case Get_id(ignore_stin_tick) : {
        models_increment=0;
        update=1;
      } break;
//Add Button
      case Get_id(add_button) : {
        Dynamic_Text model_names_add;
        View view_add;
        if(Validate(view_add_box,CHECK_VIEW_MUST_EXIST,view_add)==VIEW_EXISTS){
          View_get_models(view,model_names_add);
          View_add_model(view_add,model_names_add);
          View_redraw(view_add);
          Set_data(message_box,"Models added");
        } else {
          Set_enable(add_button,0);
          Set_enable(remove_button,0);
        }
      } break; // add_button
//Remove Button
      case Get_id(remove_button) : {
        Dynamic_Text model_names_remove;
        View view_remove;
        if(Validate(view_add_box,CHECK_VIEW_MUST_EXIST,view_remove)==VIEW_EXISTS){
          View_get_models(view,model_names_remove);
          View_remove_model(view_remove,model_names_remove);
          View_redraw(view_remove);
          Set_data(message_box,"Matching models removed");
        } else {
          Set_enable(add_button,0);
          Set_enable(remove_button,0);
        }
      } break; // remove_button
//Clean Button
      case Get_id(clean_button) : {
        Dynamic_Text model_names_clean;
        View_get_models(view,model_names_clean);
        Model_clean(model_names_clean);
        Calc_extent(view);
        View_fit(view);
        View_redraw(view);
        Set_data(message_box,"Models cleaned");
      } break; // clean_button
//Delete Button
      case Get_id(delete_button) : {
        Dynamic_Text model_names_delete;
        View_get_models(view,model_names_delete);
        Model_delete(model_names_delete);
        Calc_extent(view);
        View_fit(view);
        View_redraw(view);
        Set_data(model_box,"");
        Set_data(message_box,"Models deleted");
      } break; // delete_button
//Reset Button
      case Get_id(reset_button) : {
        update=5;
      } break; // reset_button
//Show Button
      case Get_id(show_elements) : {
        models_increment=0;
        Get_name(show_elements,my_text_return);
        if (my_text_return=="Hide filtered"){
          update=1;
          Set_name(show_elements,"Display filtered");
        } else {
          update=3;
          Set_name(show_elements,"Hide filtered");
        }
      } break;
    }  // switch id
    if(update&1){
      if(update&8){
        Get_data(model_box,model_name);
        Integer pos = Find_text(model_names,model_name);
        if(pos>0){
          display_start_pos = pos;
          display_end_pos= pos;
        } else {
          Get_project_model_names(model_names,"*",num_items,sort_box);
          pos = Find_text(model_names,model_name);
          if(pos>0){
            display_start_pos = pos;
            display_end_pos = pos;
          }
        }
      }
      View_remove_model(view);
      if(update&4)Get_project_model_names(model_names,"*",num_items,sort_box);
      Dynamic_Text my_names;
      Models_display_increment(my_names,models_increment,display_start_pos,display_end_pos, model_names);
      if(update&2){
      } else {
        Models_filter(my_names, elements_max_box, ignore_tin_tick, ignore_stin_tick, show_elements, filter_box);
      }
      Models_data(model_pos_box,model_box,model_names, display_start_pos, display_end_pos);
      Models_message(message_box, model_names, display_start_pos, display_end_pos);
      View_add_model(view,my_names);
      Calc_extent(view);
      View_fit(view);
      update=0;
    }
  }  // while !done
  return;
}

Integer Get_project_model_names(Dynamic_Text &model_names, Text models_search, Integer &num_items, Choice_Box sort_box)
{
  Integer return_result = 1;
  switch (models_search) {
    case "" : case "*" : {
      Null(model_names);
      Get_project_models(model_names);
      Get_number_of_items(model_names,num_items);
      return_result = 0;
    } break;
    default : {  //Later to be used to create a filtered project models list
      Null(model_names);
      Get_project_models(model_names);
      Get_number_of_items(model_names,num_items);
      return_result = 0;
    }
  }
  Text sort_by;
  Get_data(sort_box,sort_by);
  switch (sort_by) {
    //Date created is the default order generated by Get_project_models call, no need to sort.
    case "No. Elements" : {
      Integer count =1;
      Text model_name;
      Dynamic_Integer no_elts;
      while (Get_item(model_names,count,model_name)==0){
        Integer no_items;
        Model model = Get_model(model_name);
        Dynamic_Element elts;
        if(Get_elements(model,elts,no_items)==0){
          Append(no_items,no_elts);
        } else {
          no_items = 0;
          Append(no_items,no_elts);
        }
        count++;
      }
      Integer index[num_items];
      Qsort(no_elts,index,num_items);
      Dynamic_Text model_names_sorted;
      if(Get_sorted(model_names,index,model_names_sorted)==0){
        model_names = model_names_sorted;
      } else {
        return_result=-1;
      }
    } break;
    case "Alpha Numeric" : {
      Integer index[num_items];
      Qsort(model_names,index,num_items);
      Dynamic_Text model_names_sorted;
      if(Get_sorted(model_names,index,model_names_sorted)==0){
        model_names = model_names_sorted;
      } else {
        return_result=-1;
      }
    } break;
  }
#if DEBUG
  Print(model_names,"Project Models");
#endif
  return return_result;
}

Integer Models_display_increment(Dynamic_Text &model_names_add, Integer increment,Integer &display_start_pos,Integer &display_end_pos,Dynamic_Text &model_names)
//Model display step by models_increment:- <0 = previous, >0 = next, 0 = display models_pos
{
  Integer return_result = 1,num_items, start, end, direction;
  Integer my_return;
  Get_number_of_items(model_names,num_items);
  direction = display_end_pos - display_start_pos;
  start = display_start_pos;
  end = display_end_pos;
  if(direction>0){
    if(increment>0){
      start = display_end_pos + 1;
      if(start>num_items)start=1;
      end = start + increment - 1;
      if(end>num_items)end=num_items;
    } else if(increment<0){
      start = display_end_pos;
      end = start + increment + 1;
      if(end<1)end=1;
    }
  } else if(direction<0) {
    if(increment>0){
      start = display_end_pos;
      end = start + increment - 1;
      if(end>num_items)end=num_items;
    } else if(increment<0){
      start = display_end_pos - 1;
      if(start<1)start=num_items;
      end = start + increment + 1;
      if(end<1)end=1;
    }
  } else {
    if(increment>0){
      start = display_end_pos + 1;
      if(start>num_items)start=1;
      end = start + increment - 1;
      if(end>num_items)end=num_items;
    } else if(increment<0){
      start = display_end_pos - 1;
      if(start<1)start=num_items;
      end = start + increment + 1;
      if(end<1)end=1;
    }
  }
  display_start_pos = start;
  display_end_pos = end;
#if DEBUG
  Print(display_start_pos,"New start");
  Print(display_end_pos,"New end");
#endif
//Get model names to display
//Set start end numbers;
  if(display_start_pos<display_end_pos){
    start = display_start_pos;
    end = display_end_pos;
  }else{
    end = display_start_pos;
    start = display_end_pos;
  }
//Get model_names
  Null(model_names_add);
  while(start<=end){
    Text model_name;
    if(Get_item(model_names,start,model_name)==0){
      Append(model_name, model_names_add);
      return_result=0;
    }
    start++;
  }
#if DEBUG
  Print(model_names_add,"Models to display");
#endif
  return return_result;
}

Integer Models_filter(Dynamic_Text &model_names_org, Integer_Box elt_box, Named_Tick_Box tin_tick, Named_Tick_Box stin_tick, Button show_elements, Message_Box filter_box)
{
  Integer return_result=0, no_elts=0, count=1, nof_tins=0, nof_stins=0, nof_elts=0, nof_models=0;
  Text tin, stin, model_name, my_text;
  Dynamic_Text model_names;
//Filter models options
  Get_data(tin_tick,tin);
  Get_data(stin_tick,stin);
  Get_data(elt_box, my_text);
  From_text(my_text,no_elts);
//Remove model names as per filter models options
  if(no_elts>0 || tin=="true" || stin=="true"){
    while (Get_item(model_names_org,count,model_name)==0){
//Number of elements
      if(Is_null(no_elts)==0 && no_elts>0){
        Integer no_items;
        Model model = Get_model(model_name);
        if(Get_number_of_items(model,no_items)==0){
          if(no_elts<no_items){
            count++;
            return_result++;
            nof_models++;
            nof_elts=nof_elts+no_items;
#if DEBUG
  Print(model_name,"Model filtered, elements");
#endif
            continue;
          }
        }
      }
//Tin models / Supertin models
      if(tin=="true" || stin=="true"){
        Tin my_tin = Get_tin(model_name,1);
        if(Is_supertin(my_tin)==1 && stin=="true"){
          count++;
          return_result++;
          nof_models++;
          nof_stins++;
#if DEBUG
  Print(model_name,"Model filtered, supertin");
#endif
          continue;
        } else if(Is_null(my_tin)==0 && tin=="true"){
          count++;
          return_result++;
          nof_models++;
          nof_tins++;
#if DEBUG
  Print(model_name,"Model filtered, tin");
#endif
          continue;
        }
      }
      Append(model_name,model_names);
      count++;
    }
    model_names_org = model_names;
#if DEBUG
  Print(model_names,"Models not filtered");
#endif
    if(nof_models>0){
      Set_enable(show_elements,1);
      Set_name(show_elements,"Display filtered");
      Set_data(filter_box,To_text(nof_models)+" models,"+To_text(nof_elts)+" elts,"+To_text(nof_tins)+" tins,"+To_text(nof_stins)+" stins");
    } else {
      Set_enable(show_elements,0);
      Set_data(filter_box,"");
    }
  }
  return return_result;
}

void Models_data(Integer_Box model_pos_box, Model_Box model_box, Dynamic_Text model_names, Integer start, Integer end)
{
  Integer num_items;
  Get_number_of_items(model_names,num_items);
  Set_data(model_pos_box,end);
  Integer direction = end - start;
  Set_data(model_box,"");
  if(direction==0){
    Text model_name;
    Get_item(model_names,start,model_name);
    if(Model_exists(model_name)){
      Set_data(model_box,model_name);
    }
  }
}

void Models_message(Message_Box message_box, Dynamic_Text model_names, Integer start, Integer end)
{
  Integer num_items;
  Get_number_of_items(model_names,num_items);
  Integer direction = end - start;
  if(direction==0){
    Text model_name;
    Get_item(model_names,start,model_name);
    Integer num_elem;
    if(Model_exists(model_name)){
      Model model=Get_model(model_name);
      Get_number_of_items(model,num_elem);
      Set_data(message_box,"No. "+To_text(start) + " of " + To_text(num_items) + " " + "(" + To_text(num_elem)+" elements)");
    } else {
      Set_data(message_box,"No. "+To_text(start) + " of " + To_text(num_items) + " Model no longer exists: " + model_name);
    }
  } else if(direction>0){
    Set_data(message_box,"No. "+To_text(start)+" ->> "+To_text(end) + " of " + To_text(num_items));
  } else {
    Set_data(message_box,"No. "+To_text(end)+" <<- "+To_text(start) + " of " + To_text(num_items));
  }
}

//--------------------------------------------------------------------------------------
//------------------------------General Sub Routines------------------------------------
//--------------------------------------------------------------------------------------

//slf.H file from 12D forums
{
  Text    spacer   = "\t";
  Text    new_line = "\n";
  Integer pad = 0;
}

Text get_pad()
{
  Text P;

  for(Integer p=1;p<=pad;p++) {
    P += " ";
  }
  return(P);
}
Text indent()
{
  Text P = get_pad();
  pad += 2;
  return(P);
}
Text outdent()
{
  pad -= 2;
  Text P = get_pad();
  return(P);
}
Text quote(Text text)
{
  Text q = "\"";

  return(q + text + q);
}
Text field(Text name,Text value)
{
  return(get_pad() + "Field" + spacer + quote(name) + spacer + quote(value) + new_line);
}
Text field(Text prefix,Text name,Text value)
{
  return(get_pad() + "Field" + spacer + quote(prefix + " - " + name) + spacer + quote(value) + new_line);
}
Text group(Text name,Text value)
{
  return(indent() + name + "  " + quote(value) + " {" + new_line);
}
Text group(Text prefix,Text name,Text value)
{
  return(indent() + name + spacer + quote(prefix + " - " + value) + " {" + new_line);
}
Text group()
{
  return(outdent() + "}" + new_line);
}

//QSort.h file from 12D forums
//
//--------------------------------------------------------
// Start of "void Qsort(Dynamic_Integer &list,Integer &index[],Integer number)"
Integer get_item(Dynamic_Integer &list,Integer pos)
{
  Integer i;
  if(Get_item(list,pos,i) == 0) return(i);
// eek programming error :-(
  return(-99999999);
}
void partit (Dynamic_Integer &list, Integer &index[], Integer &i, Integer &j, Integer left, Integer right )
{
   Integer temp;
   Integer ii;
   Integer jj;
   Integer pivot;
   ii = left;
   jj = right;
   pivot = index [(ii+jj)/2];
   while ( ii <= jj )
   {
      while ( get_item(list,index[ii]) < get_item(list,pivot)     ) ++ii;
      while ( get_item(list,pivot)     < get_item(list,index[jj]) ) --jj;
      if ( ii <= jj )
      {
         temp      = index[ii];
         index[ii] = index[jj];
         index[jj] = temp;
         ++ii;
         --jj;
      }
   }
   i = ii;
   j = jj;
   return;
}
void qisort1 (Dynamic_Integer &list, Integer &index[], Integer m, Integer n)
{
   Integer i; 
   Integer j;
   if ( m < n )
   {
      partit  (list,index,i,j,m,n);
      qisort1 (list,index,m,j);
      qisort1 (list,index,i,n);
   }
   return;
}
//Name
void Qsort(Dynamic_Integer &list,Integer &index[],Integer number)
//Sort Dynamic_Integer
{
   Integer j;
   // initialise the index
   for ( j=1 ; j<=number ; ++j ) index[j]=j;
   qisort1 (list,index,1,number);
   return;
}
// End
//--------------------------------------------------------

//--------------------------------------------------------
// Start of "void Qsort(Dynamic_Text &list,Integer &index[],Integer number)"
Text get_item(Dynamic_Text &list,Integer pos)
{
  Text t;
  if(Get_item(list,pos,t) == 0) return(t);
// eek programming error :-(
  return("");
}
void partit (Dynamic_Text &list, Integer &index[], Integer &i, Integer &j, Integer left, Integer right )
{
   Integer temp;
   Integer ii;
   Integer jj;
   Integer pivot;
   ii = left;
   jj = right;
   pivot = index [(ii+jj)/2];
   while ( ii <= jj )
   {
      while ( get_item(list,index[ii]) < get_item(list,pivot)     ) ++ii;
      while ( get_item(list,pivot)     < get_item(list,index[jj]) ) --jj;
      if ( ii <= jj )
      {
         temp      = index[ii];
         index[ii] = index[jj];
         index[jj] = temp;
         ++ii;
         --jj;
      }
   }
   i = ii;
   j = jj;
   return;
}
void qisort1 (Dynamic_Text &list, Integer &index[], Integer m, Integer n)
{
   Integer i; 
   Integer j;
   if ( m < n )
   {
      partit  (list,index,i,j,m,n);
      qisort1 (list,index,m,j);
      qisort1 (list,index,i,n);
   }
   return;
}
//Name
void Qsort(Dynamic_Text &list,Integer &index[],Integer number)
// Sort Dynamic_Text Text
{
   Integer j;
   // initialise the index
   for ( j=1 ; j<=number ; ++j ) index[j]=j;
   qisort1 (list,index,1,number);
   return;
}
// End
//--------------------------------------------------------


//
//Developed by Lucien West, Cardno (Qld) Pty Ltd - Civil Designer, Springfield
//

//General debug tools.
//
//Name
void Print(Integer i, Text extension)
//Print each Text item to the output window with Text extension
{
  Print("0. "+To_text(i)+" -"+extension+"\n");
}
//Name
void Print(Text text, Text extension)
//Print each Text item to the output window with Text extension
{
  Print("0. "+text+" -"+extension+"\n");
}

//Name
void Print(Dynamic_Text dtext, Text extension)
//Print each Text item in dtext to the output window with Text extension
{
  Integer count=1;
  Text text;
  Print("DEBUG-"+extension+"\n");
  while (Get_item(dtext,count,text)==0){
    Print(To_text(count)+". "+text+" -"+extension+"\n");
    text="";
    count++;
  }
}

//General Text Tools.
//
//Name
Integer Find_text(Dynamic_Text dtext,Text tofind)
//Description
//Find the first whole match of the Text tofind within the items of Dynamic_Text dtext.
//If tofind exists within items of dtext, the position of tofind is returned as the function return value.
//If tofind does not exist within text, a start position of zero is returned as the function return value.
//Hence a function return value of zero indicates the Text tofind does not exist within the Dynamic_Text dtext.
{
  Integer return_result=0, count=1;;
  Text test;
  while (Get_item(dtext,count,test)==0) {
    if(tofind==test){
      return_result=count;
      break;
    }
    count++;
  }
  return return_result;
}

//Name
Integer Get_sorted(Dynamic_Text list, Integer index[], Dynamic_Text &list_sorted)
//Description
//
{
  Integer return_result=1, count=1;
  Integer no_items1, no_items2;
  Get_number_of_items(list,no_items1);
  Text my_text;
  Null(list_sorted);
  for (count=1;count<=no_items1;count++){
    Get_item(list,index[count],my_text);
    Append(my_text,list_sorted);
  }
  Get_number_of_items(list_sorted,no_items2);
#if DEBUG
  Print(no_items1,"List length");
  Print(no_items2,"List_Sorted length");
#endif
  if(no_items1==no_items2)return_result=0;
  return return_result;
}

//General Model handling functions.
//
//Name
Integer Model_clean(Model model)
//Description
//Delete all elements in the Model model.
//A function return value of zero indicates the model was successfully cleaned.
{
  Integer count=1, num_elts;
  if(Model_exists(model)==0){
    Dynamic_Element elts;
    Element elt;
    Get_elements(model,elts,num_elts);
    while(Get_item(elts,count,elt)==0){
      Element_delete(elt);
      count++;
    }
  }
  num_elts=1;
  Get_number_of_items(model,num_elts);
  return num_elts;
}

//Name
Integer Model_clean(Dynamic_Text model_names)
//Description
//Clean all elements in the Models listed in model_names.
//A function return value of zero indicates one or more of the models was successfully cleaned.
{
  Integer return_result=1, count=1, num_elts;
  Text model_name;
  while(Get_item(model_names,count,model_name)==0){
    if(Model_exists(model_name)==1){
      Model model = Get_model(model_name);
      Integer count2=1;
      Dynamic_Element elts;
      Element elt;
      Get_elements(model,elts,num_elts);
      while(Get_item(elts,count2,elt)==0){
        Element_delete(elt);
        count2++;
      }
      num_elts=1;
      Get_number_of_items(model,num_elts);
    }
    if(num_elts==0) return_result=0;
    count++;
  }
#if DEBUG
  Print(model_names,"Model to clean");
#endif
  return return_result;
}

//Name
Integer Model_delete(Dynamic_Text model_names)
//Description
//Delete all Models listed in model_names.
//A function return value of zero indicates one or more of the models was successfully deleted.
{
  Integer return_result=1, count=1;
  Text model_name;
  while(Get_item(model_names,count,model_name)==0){
    if(Model_exists(model_name)==1){
      Model model = Get_model(model_name);
      Integer count2=1, num_elts;
      Model_delete(model);
      if(Model_exists(model_name)==0) return_result=0;
    }
    count++;
  }
#if DEBUG
  Print(model_names,"Model to delete");
#endif
  return return_result;
}

//General view handling functions.
//
//Name
Integer View_exists(Text &view_name,Text type,Integer mode)
//Description
//Checks to see if a view with the name view_name and View Type type exists using options mode.
//Standard View_exist call is case sensitive, unlike Model_exists.
//Views can have same name different case. ie "Long" vs "LONG"
//The available modes are
//0 = standard View_exits, 1 = check case variation ,  2 = return existing name
//A non-zero function return value indicates a view does exist of name and type.
//A zero function return value indicates that no view of that name and type combination exists.
//Warning - this is the opposite of most 4DML function return values
{
  Integer return_result = 0;
  switch (mode){
    case 0 : {
      return_result = View_exists(view_name);
    } break;
    case 1 : case 3 : {
      Dynamic_Text view_tests;
      if(!Get_project_views(view_tests)){
        Integer count, num_items;
        Text view_test,type_test;
        View view;
        Get_number_of_items(view_tests,num_items);
        for (count=0; count <= num_items; count++) {
          Get_item(view_tests,count,view_test);
          if (Text_upper(view_name) == Text_upper(view_test)){
            view = Get_view(view_test);
            Get_type(view,type_test);
            if (Text_upper(type_test) == Text_upper(type) || type==""){
              if (mode&2)view_name=view_test;
              return_result = 1;
              count = num_items + 1;
            }
          }
        }
      }
    } break;
  }
  return return_result;
}

//Name
Integer View_remove_model(View view,Text model_name)
//Description
//Remove the Model model_name from the View view.
//A function return value of zero indicates that the model was successfully removed from the view.
{
  Integer return_result = 1;
  if (Model_exists(model_name)){
   	Model model = Get_model(model_name);
    return_result = View_remove_model(view,model);
  }
  return return_result;
}

//Name
Integer View_remove_model(View view, Dynamic_Text model_names)
//Description
//Remove all the Models in Dynamic_Text model_names from the View view.
//A function return value of zero indicates that one or more models were removed from the view.
{
  Integer return_result = 1;
  Integer count, num_items;
  Text model_name;
  Model model;
  Get_number_of_items(model_names,num_items);
  for (count=0; count <= num_items; count++) {
    Get_item(model_names,count,model_name);
    if (Model_exists(model_name)){
      model = Get_model(model_name);
      if (View_remove_model(view,model)==0){return_result = 0;}
    }
  }
  return return_result;
}

//Name
Integer View_remove_model(View view)
//Description
//Removes all Models from View view.
//**Note: - Requires above function - Integer View_remove_model(View view, Dynamic_Text model_names).
//A function return value of zero indicates that all models were removed from the view.
{
  Integer return_result = 1;
  Dynamic_Text model_names;
  View_get_models(view,model_names);
  View_remove_model(view,model_names);
  Null(model_names);
  View_get_models(view,model_names);
  Get_number_of_items(model_names,return_result);
  return return_result;
}

//Name
Integer View_add_model(View view,Text model_name)
//Description
//Add the Model model to the View view_name.
//A function return value of zero indicates that the model was successfully added to the view.
{
  Integer return_result = 1;
  if (Model_exists(model_name) ) {
   	Model model = Get_model(model_name);
    return_result = View_add_model(view,model);
  }
  return return_result;
}

//Name
Integer View_add_model(View view, Dynamic_Text model_names)
//Description
//Add all the Models in Dynamic_Text model_names to the View view.
//A function return value of zero indicates that one or more models were added to the view.
{
  Integer return_result = 1;
  Integer count, num_items;
  Text model_name;
  Model model;
  Get_number_of_items(model_names,num_items);
  for (count=0; count <= num_items; count++) {
    Get_item(model_names,count,model_name);
    model = Get_model(model_name);
    if (!View_add_model(view,model)){return_result = 0;}
  }
  return return_result;
}

//Name
Integer View_get_next(Text &number_name)
//Description
//Get the first available View number_name, limited to 20.
//A non-zero function return value indicates successfull View number_name, is also the Integer of number_name.
//A zero function return value indicates unsuccessfull View number_name.
//Warning - this is the opposite of most 4DML function return values.
{
  Integer return_result = 0;
  Integer count = 1;
  Text view_test;
  do {
    view_test = To_text(count);
    if (!View_exists(view_test)) {
      number_name = view_test;
      return_result = count;
      count = 20;
    }
    count++;
  } while (count < 20);
  return return_result;
}

//Name
Integer Create_view(Text view_name,Text type)
//Description
//Create a View view_name of Type type.
//The type is
//"Plan" if the view is a plan view.
//"Section"	section view.
//"Perspective"	perspective view.
//"Perspective OpenGL" OpenGL perspective view.
//**Note: non functioning view "Hidden_perspective" hidden perspective view.
//**Note: - Requires function from libray file include - "slf.H"
//A function return value of zero indicates that the was created successfully.
{
  Integer return_result = 1;
  if(!View_exists(view_name)){
    Text panel_name;
    Text data;
    switch (Text_lower(type)) {
      case "plan" : {
        panel_name = "New Plan View";
        break;
      }
      case "section" : {
        panel_name = "New Section View";
        break;
      }
      case "perspective" : {
        panel_name = "New Perspective View";
        break;
      }
      case "perspective opengl" : {
        panel_name = "New Perspective OpenGL View";
        break;
      }
//      case "hidden_perspective" : {
//        panel_name = "New Hidden_perspective View";
//        break;
//      }
    }
    if (panel_name!=""){
      Integer interactive = 0;
#if INTERACTIVE
    interactive = 1;
#endif
      data += field("View name",view_name);
      return_result = Panel_prompt(panel_name,interactive,data);
//Check if view now exists
      if (View_exists(view_name)==1) {return_result = 0;}
    }
  }
  return return_result;
}

//Name
Integer Create_view(Text &view_name,Text &type,Integer mode)
//Description
//Create a View view_name or if view_name is "" next avaialable view number, of Type type using options mode.
//**Note: - Requires above functions 
// - Integer View_exists(Text view_name,Integer mode)
// - Integer View_exists(Text view_name,Text type,Integer mode)
// - Integer View_remove_model(View view)
// - Integer Create_view(Text view_name,Text type)
//**Note: - Requires function from libray file include- "CDD-Message_Panels.H"
// - Integer Message_view_panel(Text title,Text message,Text &view_name)
//The available types are
//Perspective, OpenGL, Section, Plan (Default if blank or doesn't match types listed)
//The available modes are
//0 = Create if not existng
//1 = Check ignoring text case
//2 = Overrides check text case if the view type is different (no effect if 1 not set)
//4 = Prompt for new name if existing
//8 = If existing clear models and ignore prompt for new name
//A function return value of zero indicates that the view was created or models cleared successfully, 
// view_name updated if prompt for new name successfull.
{
  Integer return_result = 1;
  Text view_original = view_name;
//Check if view_name not blank and view_name not exisitng
start:
  if (!View_exists(view_name)) {
    if (view_name == "") {
      //If no view use next available view number
      if (View_get_next(view_name)) {
        return_result = Create_view(view_name,type);
      }
    } else {
      if (mode&1){
        //If mode 1 - check text case of view name
        if (View_exists(view_name,"",3)) {
          //Exisitng and mode 2 - check view type before getting new name
          if (mode&2) {
            if (View_exists(view_name,type,3)){
              //If type same then get new name
              goto new_name;
            } else {
              //If type different create
              view_name = view_original;
              return_result = Create_view(view_name,type);
            }
          } else {
            //Existing view name get new name
            goto new_name;
          }
        } else {
          //View name ignoring case not existing create view
          return_result = Create_view(view_name,type);
        }
      } else {
        //View not existing create view
        return_result = Create_view(view_name,type);
      }
    }
  } else {
    goto new_name;
  }
  goto end;
  new_name:
     if (mode&8){
      View view = Get_view(view_name);
      return_result = View_remove_model(view);
    } else {
      if (mode&4){
        if(Message_view_panel("Create View","View "+view_name+" exists, new name?\n",view_name)){goto start;}
      } else {
      }
    }
  end:
  return return_result;
}

//General tin handling functions.
//
//Name
Integer Is_null(Tin tin)
//Description
//Checks to see if the Tin tin is null or not.
//A non-zero function return value indicates the tin is null.
//A zero function return value indicates the tin is not null.
//Warning - this is the opposite of most 4DML function return values
{
  Integer return_result = 0;
  Uid id;
  Get_id(tin,id);
  return_result = Is_null(id);
  return return_result;
}

//Name
Integer Is_supertin(Tin stin)
//Description
//Checks to see if the Tin stin is of type SuperTin.
//A non-zero function return value indicates that stin_name is a supertin.
//A zero function return value indicates that stin_name does not exist or is a standard tin.
//Warning - this is the opposite of most 4DML function return values
{
  Integer return_result=0;
  if (Tin_exists(stin)==1){
    Text type;
    Get_type(stin,type);
    if(type=="SuperTin")return_result=1;
  }
  return return_result;
}

//Name
Tin Get_tin(Text model_name,Integer from_model)
//Description
//Get the first Tin in the Model model_name.
//**Note: from_model value not used.
//If there is a tin in the model, the handle to it is returned as the function return value.
//If there is no tin in the model, a null Tin is returned as the function return value.
{
  Integer count;
  Model model;
  Dynamic_Element model_elements;
  Element element;
  Text type;
  Tin tin;
  Null(tin);
  if (Model_exists(model_name)){
    model = Get_model(model_name);
    if (Get_elements(model,model_elements,count)==0){
      count = 1;
      while (Get_item(model_elements,count,element)==0) {
      Get_type(element,type);
        if (type == "Tin" || type == "SuperTin"){
          Get_name(element,type);
          tin = Get_tin(type);
          break;
        }
        count++;
      }
    }
  }
  return tin;
}

//General message panels.
//
//Name
Integer Message_view_panel(Text title,Text message,Text &view_name)
//Description
//New View Replica panel, used to capture result for macro processing.
//View select panel with Title title, Message message and Selected View view_name.
//A function return value of zero indicates that a view_name was entered successfully, view_name updated.
{
  Integer return_result = 0;
//
//Create the panel
//
  Text macro_help = "help";
  Message_Box message_box = Create_message_box(title);
//
  Panel panel = Create_panel(title);
  Vertical_Group vgroup = Create_vertical_group(0);
//Screen Text
  Screen_Text screen_text = Create_screen_text(message);
  Set_enable(screen_text,1);
  Append(screen_text,vgroup);
//Box View
  View_Box view_box = Create_view_box("View name",message_box,CHECK_VIEW_MUST_NOT_EXIST);
  Set_help(view_box,macro_help);
  Append(view_box,vgroup);
  Set_width_in_chars(view_box,15);
  Set_data(view_box,view_name);
//Button Horixontal Group
  Horizontal_Group button_group = Create_button_group();
//Buttton Proccess
  Button create_button = Create_button("Create","create");
  Set_help(create_button,macro_help);
  Append(create_button,button_group);
//Button Finish
  Button finish_button = Create_finish_button("Finish","finish");
  Set_help(finish_button,macro_help);
  Append(finish_button,button_group);
//Button Finish
  Button help_button = Create_button("Help","help");
  Set_help(help_button,macro_help);
  Append(help_button,button_group);
//Box Message - Created before Widgets using
  Append(message_box,vgroup);
//Panel Finish
  Append(button_group,vgroup);
  Append(vgroup,panel);
// Display Panel
//
  Show_widget(panel);
//
  Integer panel_run = 1;
  Integer panel_cancel = 1;
  Integer ret_int;
  Text ret_text;
  Set_focus(view_box);
  while (panel_run) {
    Integer id,ierr;
    Text cmd,msg;
    Wait_on_widgets(id,cmd,msg);
//#if DEBUG
//Print("panel id  <"+To_text(id)+"> cmd <"+cmd+"> msg <"+msg+">\n");
//#endif
//first process the commands that are common to all wgits or are rarely processed by the wigit ID
    switch(cmd) {
      case "kill_focus" : {
       continue;
      } break;
      case "Help" : {
        Winhelp(panel,"4d.hlp",'a',msg);
        continue;
      } break;
    }
//process each event by the wigit id
//most wigits do not need to be processed until the PROCESS button is pressed
//only the ones that change the appearance of the panel need to be processed in this loop
    switch(id) {
//Panel Quit Exit
      case Get_id(panel) :{
        if(cmd == "Panel Quit") {
          panel_run = 0;
      }
        if(cmd == "Panel About") {continue;}
        break;
      }
//Finish Create Exit
      case Get_id(create_button) : {
        panel_run = 0;
        panel_cancel = 0;
        break;
      }
//Finish Button Exit
      case Get_id(finish_button) : {
        panel_run = 0;
        break;
      }
//Type Box
      case Get_id(view_box) : {
//#if DEBUG
//Print("view id  <"+To_text(Get_id(view_box))+"> cmd <"+cmd+"> msg <"+msg+">\n");
//#endif
        switch (cmd) {
          case "keystroke" : {
            Get_char(msg, 1, ret_int);
            if(ret_int == 13) {
              Get_data(view_box,ret_text);
              if(View_exists(ret_text)) {
                Set_data(message_box,"View must not exist");
              } else {
                panel_run = 0;
                panel_cancel = 0;
              }
            }
          break;
          }
        }
      }
    }  // switch id
  }  // while !done
  if (panel_cancel==0) {
    Get_data(view_box,view_name);
    return_result = 1;
  }
  return return_result;
}